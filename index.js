const express = require('express');
const path = require('path')
const cors = require('cors');
const monk = require('monk');
const helmet = require('helmet')

require('dotenv').config();

const db = monk(process.env.MONGO_URI);
const talks = db.get('talks');

const app = express();
app.use(helmet());
app.use(cors({ origin: process.env.CORS_ORIGIN }))
app.use(express.static(path.join(__dirname, 'build')));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// app.get('/', (req, res) => {
//     res.send('Backend of Talkify...')
// })

app.get('/talks', (req, res) => {
    talks
        .find()
        .then(talks => res.send(talks))
})

app.use(express.json())
app.post('/talks', (req, res) => {
    const name = req.body.name.toString();
    const content = req.body.content.toString();
    const newTalk = {
        name,
        content
    }

    talks
        .insert(newTalk)
        .then(newTalk => {
            res.send(newTalk)
        })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`Listening on ${PORT}`));